package com.gambungstore.seller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gambungstore.seller.adapter.PesananAdapter;
import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.DetailPesanan;
import com.gambungstore.seller.model.MasterDetailPesanan;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentPesanan extends Fragment {

    private static final String TAG = "FragmentPesanan";

    private List<DetailPesanan> pesananList;
    private Service service;
    private RecyclerView mRecyclerView;
    private PesananAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ProgressBarGambung progressbar;

    String from_date = null, until_date = null;
    
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_pesanan, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressbar = new ProgressBarGambung(getActivity());
        progressbar.startProgressBarGambung();

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        this.service = Client.getClient(Client.BASE_URL).create(Service.class);
        this.getAdapter();

        EditText mSearch = getActivity().findViewById(R.id.search);
        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAdapter.getFilter().filter(charSequence.toString());
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        Button btnPeriode = getActivity().findViewById(R.id.periodeButton);
        btnPeriode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PeriodeActivity.class);
                startActivityForResult(intent,1);
            }
        });
    }

    private void getAdapter(){
        Call<MasterDetailPesanan> dataPesanan = service.getPesanan(
                SharedPreference.getRegisteredStore(getContext())
        );
        dataPesanan.enqueue(new Callback<MasterDetailPesanan>() {
            @Override
            public void onResponse(Call<MasterDetailPesanan> call, Response<MasterDetailPesanan> response) {
                pesananList = response.body().getDetailPesanan();
                if (pesananList != null) {
                    mAdapter = new PesananAdapter(pesananList, requireContext(), getFragmentManager(), from_date, until_date);
                    mRecyclerView.setAdapter(mAdapter);
                }
                progressbar.endProgressBarGambung();
            }

            @Override
            public void onFailure(Call<MasterDetailPesanan> call, Throwable t) {
                Toast.makeText(getContext(), "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progressbar.endProgressBarGambung();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getAdapter();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if(resultCode == getActivity().RESULT_OK) {
                this.from_date = data.getStringExtra("from_date");
                this.until_date = data.getStringExtra("until_date");
                this.getAdapter();
            }
        }
    }

}
