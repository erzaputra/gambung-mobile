package com.gambungstore.seller;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.Rajaongkir;
import com.gambungstore.seller.model.ResultCities;
import com.gambungstore.seller.model.Users;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentProfile extends Fragment{
    private static final String TAG = "FragmentProfile";

    private String kodeKota = null;

    private EditText nama,kota,tgl_lahir,alamat,handphone,email;
    private Spinner cities;

    private Service service;

    private List<Users> user;

    private Rajaongkir RajaongkirCities;

    private ProgressBarGambung progressbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profil, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressbar = new ProgressBarGambung(getActivity());
        progressbar.startProgressBarGambung();

        this.service = Client.getRajaongkir().create(Service.class);
        this.showData();

        Button btn_logout = getActivity().findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                logout();
                Toast.makeText(getContext(), "Berhasil Logout", Toast.LENGTH_LONG).show();
            }
        });

        Button btn_editProfile = getActivity().findViewById(R.id.btn_editProfile);
        btn_editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });

        Button btn_ubahPassword = getActivity().findViewById(R.id.btn_ubahPassword);
        btn_ubahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditPasswordActivity.class);
                startActivity(intent);
            }
        });

    }

    private void showData(){
            Service serviceUser = Client.getClient(Client.BASE_URL).create(Service.class);

            Call<List<Users>> dataUsers = serviceUser.getUserByID(
                    SharedPreference.getRegisteredId(getContext())
            );

            dataUsers.enqueue(new Callback<List<Users>>() {
                @Override
                public void onResponse(Call<List<Users>> call, Response<List<Users>> response) {
                    user = response.body();

                    nama = getView().findViewById(R.id.et_namaUser);
                    tgl_lahir = getView().findViewById(R.id.et_tglLahir);
                    alamat = getView().findViewById(R.id.et_alamat);
                    handphone = getView().findViewById(R.id.et_handphone);
                    email = getView().findViewById(R.id.et_email);
                    cities = getView().findViewById(R.id.et_kota);

                    nama.setText(user.get(0).getName());
                    tgl_lahir.setText(user.get(0).getBirthday());
                    alamat.setText(user.get(0).getAddress_1());
                    handphone.setText(user.get(0).getPhone());
                    email.setText(user.get(0).getEmail());

                    Call<Rajaongkir> dataCities = service.getRajaongkir();
                    dataCities.enqueue(new Callback<Rajaongkir>() {
                        @Override
                        public void onResponse(Call<Rajaongkir> call, Response<Rajaongkir> response) {
                            RajaongkirCities = response.body();
                            ArrayList<String> SpinnerCities = new ArrayList<>();

                            boolean ketemu = false;
                            for(ResultCities rs : RajaongkirCities.getCities().getResult()){
                                SpinnerCities.add(rs.getCity_name());
                                if(Integer.toString(rs.getCity_id()).equals(user.get(0).getCity())){
                                    kodeKota = Integer.toString(rs.getCity_id());
                                    ketemu = true;
                                }
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                                    android.R.layout.simple_spinner_item, SpinnerCities);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            cities.setAdapter(adapter);

                            if(ketemu){
                                cities.setSelection(Integer.valueOf(kodeKota));
                            }

                            cities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                    if (kodeKota == null) {
                                        kodeKota = "22";
                                    } else {
                                        kodeKota = Integer.toString(RajaongkirCities.getCities().getResult().get(position-1).getCity_id());
                                        Log.d(TAG, "onItemSelected() returned: " + kodeKota);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parentView) {
                                    return;
                                }
                            });

                            progressbar.endProgressBarGambung();

                        }

                        @Override
                        public void onFailure(Call<Rajaongkir> call, Throwable t) {
                            Toast.makeText(getContext(), "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                            progressbar.endProgressBarGambung();
                        }
                    });

                }

                @Override
                public void onFailure(Call<List<Users>> call, Throwable t) {
                    Toast.makeText(getContext(), "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                    progressbar.endProgressBarGambung();
                }
            });

    }

    private void logout(){
        progressbar.startProgressBarGambung();
        SharedPreference.clearRegisteredId(getContext());
        SharedPreference.clearRegisteredStore(getContext());
        SharedPreference.clearRegisteredToken(getContext());
        Intent move = new Intent(getContext(), LoginActivity.class);
        move.putExtra("logout", true);
        startActivity(move);
        getActivity().finish();
        progressbar.endProgressBarGambung();
    }

    private void updateProfile(){

        progressbar.startProgressBarGambung();

        String nama = this.nama.getText().toString();
        String handphone = this.handphone.getText().toString();
        String tgl_lahir = this.tgl_lahir.getText().toString();
        String alamat = this.alamat.getText().toString();



        if (nama == null || handphone == null || tgl_lahir == null || alamat == null || kodeKota == null){
            Toast.makeText(getContext(), "Data belum diisi", Toast.LENGTH_SHORT).show();
            return;
        }



        int user = SharedPreference.getRegisteredId(getContext());

        Log.d(TAG, "updateProfile: "+nama+" "+handphone+" "+tgl_lahir+" "+alamat+" "+kodeKota+" "+user);
        Service userService = Client.getClient(Client.BASE_URL).create(Service.class);
        Call<String> updateProfile = userService.editProfile(
                Integer.toString(user),
                this.nama.getText().toString(),
                this.handphone.getText().toString(),
                this.alamat.getText().toString(),
                Integer.valueOf(kodeKota),
                Date.valueOf(this.tgl_lahir.getText().toString())
        );
        updateProfile.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d(TAG, "onResponse: "+response.raw());
                Toast.makeText(getContext(), "Berhasil diubah", Toast.LENGTH_SHORT).show();
                getActivity().finish();
                startActivity(getActivity().getIntent());
                progressbar.endProgressBarGambung();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (t.getMessage().matches("JSON document was not fully consumed.")){
                    Toast.makeText(getContext(), "Berhasil diubah", Toast.LENGTH_SHORT).show();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.detach(FragmentProfile.this).attach(FragmentProfile.this).commit();
                    progressbar.endProgressBarGambung();
                }else{
                    Toast.makeText(getContext(), "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                    progressbar.endProgressBarGambung();
                }
            }
        });
    }
}