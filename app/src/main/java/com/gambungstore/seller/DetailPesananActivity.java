package com.gambungstore.seller;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gambungstore.seller.adapter.DetailPesananAdapter;
import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.DetailPesanan;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPesananActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "DetailPesananActivity";

    private ImageButton btnBack;
    private TextView mTotal,mExpedition,mPembeli,mAlamat,mTelphone;
    private Button mConfirm;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Service service;

    private List<DetailPesanan> detailpesanan;

    ProgressBarGambung progresbar;
    String noResi;

    public int total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pesanan);

        progresbar = new ProgressBarGambung(this);
        progresbar.startProgressBarGambung();

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);

        mTotal = findViewById(R.id.tv_totalHarga);
        mExpedition = findViewById(R.id.tv_expedition);
        mAlamat = findViewById(R.id.tv_alamatPengiriman);
        mPembeli = findViewById(R.id.tv_dikirimKe);
        mTelphone = findViewById(R.id.tv_telphone);
        mConfirm = findViewById(R.id.btn_detailConfirm);

        mRecyclerView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        this.service = Client.getClient(Client.BASE_URL).create(Service.class);
        this.getAdapter();

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_back:
                finish();
                break;
        }
    }

    public void getAdapter(){
        Call<List<DetailPesanan>> dataDetail = service.getAllPesanan();
        dataDetail.enqueue(new Callback<List<DetailPesanan>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<DetailPesanan>> call, Response<List<DetailPesanan>> response) {
                detailpesanan = response.body();
                String transaction_code = getIntent().getStringExtra("transaction_code");
                int transaction_detail_id = getIntent().getIntExtra("transaction_detail_id", 0);
                ArrayList<DetailPesanan> dataPesanan = new ArrayList<>();
                for(DetailPesanan dp : detailpesanan){
                    if(dp.getTransaction_code().equals(transaction_code)){
                        dataPesanan.add(dp);
                    }
                }
                mAdapter = new DetailPesananAdapter(dataPesanan,DetailPesananActivity.this);
                mRecyclerView.setAdapter(mAdapter);

                mAlamat.setText(dataPesanan.get(0).getPesanan().getAlamat());
                mPembeli.setText("Dikirim kepada "+dataPesanan.get(0).getPesanan().getUsername());
                mExpedition.setText(dataPesanan.get(0).getExpedition());
                mTelphone.setText("Telp "+dataPesanan.get(0).getPesanan().getPhone());

                if(!dataPesanan.get(0).getShipping_status().equals("OPTNO")){
                   mConfirm.setText("Sudah Konfirmasi");
                   mConfirm.setEnabled(false);
                   mConfirm.setBackgroundResource(R.color.colorPrimary);
                }else{
                    mConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                            builder.setTitle("Masukan Nomor Resi");

                            // Set up the input
                            final EditText input = new EditText(v.getContext());
                            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
                            builder.setView(input);

                            // Set up the buttons
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    noResi = input.getText().toString();
                                    confirmation(transaction_code, transaction_detail_id, noResi);
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();
                        }
                    });
                }

                //check kadaluarsa
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime verified_date = LocalDateTime.parse(dataPesanan.get(0).getPesanan().getPayment().getVerified_date(),formatter);
                LocalDateTime now = LocalDateTime.now();
                long days = ChronoUnit.DAYS.between(verified_date, now);
                Log.d(TAG, "onResponse: "+days);
                if (days >= 1){
                    mConfirm.setText("Kadaluarsa");
                    mConfirm.setEnabled(false);
                    mConfirm.setBackgroundResource(R.color.colorPrimary);
                }

                progresbar.endProgressBarGambung();
            }

            @Override
            public void onFailure(Call<List<DetailPesanan>> call, Throwable t) {
                Toast.makeText(DetailPesananActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progresbar.endProgressBarGambung();
            }
        });
    }

    private void confirmation(String transaction_code, int transaction_detail_id, String shipping_no){

        Log.d(TAG, "confirmation token: "+ SharedPreference.getRegisteredToken(this));

        Service service = Client.getClient(Client.BASE_URL).create(Service.class);
        Call<ResponseBody> confirm = service.confirmPesanan(
                "Bearer "+SharedPreference.getRegisteredToken(this),
                transaction_code,
                transaction_detail_id,
                shipping_no
        );

        confirm.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(DetailPesananActivity.this, "Berhasil Dikonfirmasi", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(DetailPesananActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progresbar.endProgressBarGambung();
            }
        });
    }

    public void setTotal(int total){
        this.total += total;
        mTotal.setText("Rp "+total+",-");
    }
}
