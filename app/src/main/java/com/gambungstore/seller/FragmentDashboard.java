package com.gambungstore.seller;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gambungstore.seller.adapter.DashboardAdapter;
import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.Dashboard;
import com.gambungstore.seller.model.DetailPesanan;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDashboard extends Fragment {

    private static final String TAG = "FragmentDashboard";

    private Service service;
    private RecyclerView mRecyclerView;
    private DashboardAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<DetailPesanan> listPesanan;

    private TextView mtotal_transaksi_angka, mjumlah_berhasil, mjumlah_gagal, mbulan_transaksi, mbulan_transaksi_berhasil, mbulan_transaksi_gagal, bulan_transaksi_pending, jumlah_transaksi_pending;

    private ProgressBarGambung progresbar;

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progresbar = new ProgressBarGambung(getActivity());
        progresbar.startProgressBarGambung();

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        service = Client.getClient(Client.BASE_URL).create(Service.class);
        getTransaction();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    private void getTransaction(){
        Log.d(TAG, "getTransaction: "+SharedPreference.getRegisteredToken(getContext()));
        Call<Dashboard> dataDashboard = service.getDashboard(
                "Bearer "+SharedPreference.getRegisteredToken(getContext())
        );
        dataDashboard.enqueue(new Callback<Dashboard>() {
            @Override
            public void onResponse(Call<Dashboard> call, Response<Dashboard> response) {
                mtotal_transaksi_angka = getActivity().findViewById(R.id.total_transaksi_angka);
                mjumlah_berhasil = getActivity().findViewById(R.id.jumlah_berhasil);
                mjumlah_gagal = getActivity().findViewById(R.id.jumlah_gagal);
                mbulan_transaksi = getActivity().findViewById(R.id.bulan_transaksi_total);
                mbulan_transaksi_berhasil = getActivity().findViewById(R.id.bulan_transaksi_berhasil);
                mbulan_transaksi_gagal = getActivity().findViewById(R.id.bulan_transaksi_gagal);
                bulan_transaksi_pending = getActivity().findViewById(R.id.bulan_transaksi_pending);
                jumlah_transaksi_pending = getActivity().findViewById(R.id.total_transaksi_pending);

                Dashboard dashboard = response.body();
                mtotal_transaksi_angka.setText(String.valueOf(dashboard.getTotal_transaction()));
                mjumlah_gagal.setText(String.valueOf(dashboard.getFail_transaction()));
                mbulan_transaksi.setText(dashboard.getPeriod());
                mbulan_transaksi_berhasil.setText(dashboard.getPeriod());
                mbulan_transaksi_gagal.setText(dashboard.getPeriod());
                bulan_transaksi_pending.setText(dashboard.getPeriod());
                mjumlah_berhasil.setText(String.valueOf(dashboard.getSuccess_transaction()));
                jumlah_transaksi_pending.setText(String.valueOf(dashboard.getPending_transaction()));

                listPesanan = response.body().getLast_transaction().getDetailPesanan();
                mAdapter = new DashboardAdapter(listPesanan, requireContext());
                mRecyclerView.setAdapter(mAdapter);

                progresbar.endProgressBarGambung();


            }

            @Override
            public void onFailure(Call<Dashboard> call, Throwable t) {
                Toast.makeText(getContext(), "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progresbar.endProgressBarGambung();
            }
        });
    }
}
