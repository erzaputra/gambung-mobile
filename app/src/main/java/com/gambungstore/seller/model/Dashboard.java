package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

public class Dashboard {

    @SerializedName("period")
    private String period;
    @SerializedName("total_transaction")
    private int total_transaction;
    @SerializedName("success_transation")
    private int success_transaction;
    @SerializedName("fail_transation")
    private int fail_transaction;
    @SerializedName("pending_transation")
    private int pending_transaction;
    @SerializedName("last_transaction")
    private MasterDetailPesanan last_transaction;

    public int getPending_transaction() {
        return pending_transaction;
    }

    public void setPending_transaction(int pending_transaction) {
        this.pending_transaction = pending_transaction;
    }

    public MasterDetailPesanan getLast_transaction() {
        return last_transaction;
    }

    public void setLast_transaction(MasterDetailPesanan last_transaction) {
        this.last_transaction = last_transaction;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public int getTotal_transaction() {
        return total_transaction;
    }

    public void setTotal_transaction(int total_transaction) {
        this.total_transaction = total_transaction;
    }

    public int getSuccess_transaction() {
        return success_transaction;
    }

    public void setSuccess_transaction(int success_transaction) {
        this.success_transaction = success_transaction;
    }

    public int getFail_transaction() {
        return fail_transaction;
    }

    public void setFail_transaction(int fail_transaction) {
        this.fail_transaction = fail_transaction;
    }
}
