package com.gambungstore.seller.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rajaongkir {

    @SerializedName("rajaongkir")
    @Expose
    private Cities cities;

    public Cities getCities() {
        return cities;
    }

    public void setCities(Cities cities) {
        this.cities = cities;
    }
}
