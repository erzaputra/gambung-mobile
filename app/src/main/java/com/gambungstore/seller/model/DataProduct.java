package com.gambungstore.seller.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataProduct implements Parcelable {
    @SerializedName("store_code")
    private String store_code;
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String desctiption;
    @SerializedName("stock")
    private int stock;
    @SerializedName("color")
    private String color;
    @SerializedName("price")
    private String price;
    @SerializedName("weight")
    private int weight;
    @SerializedName("main_category")
    private String category;
    @SerializedName("code")
    private String code;
    @SerializedName("images")
    private List<ProductImages> image;
    @SerializedName("store")
    private Store store;

    public List<ProductImages> getImage() {
        return image;
    }

    public void setImage(List<ProductImages> image) {
        this.image = image;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    protected DataProduct(Parcel in) {
        store_code = in.readString();
        id = in.readInt();
        name = in.readString();
        desctiption = in.readString();
        stock = in.readInt();
        color = in.readString();
        price = in.readString();
        category = in.readString();
        weight = in.readInt();
        code = in.readString();
    }

    public static final Creator<DataProduct> CREATOR = new Creator<DataProduct>() {
        @Override
        public DataProduct createFromParcel(Parcel in) {
            return new DataProduct(in);
        }

        @Override
        public DataProduct[] newArray(int size) {
            return new DataProduct[size];
        }
    };

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesctiption() {
        return desctiption;
    }

    public void setDesctiption(String desctiption) {
        this.desctiption = desctiption;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(store_code);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(desctiption);
        dest.writeInt(stock);
        dest.writeString(color);
        dest.writeString(price);
        dest.writeString(category);
        dest.writeInt(weight);
        dest.writeString(code);
    }
}
