package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

public class CategoryStatus {

    @SerializedName("status")
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
