package com.gambungstore.seller.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DetailPesanan implements Parcelable {

    @SerializedName("id")
    public int id;
    @SerializedName("transaction_code")
    public String transaction_code;
    @SerializedName("product_code")
    public String product_code;
    @SerializedName("expedition")
    public String expedition;
    @SerializedName("quantity")
    public int quantity;
    @SerializedName("weight")
    public int weight;
    @SerializedName("price")
    public int price;
    @SerializedName("message")
    public String message;
    @SerializedName("shipping_status")
    public String shipping_status;
    @SerializedName("shipping_no")
    public String shipping_no;
    @SerializedName("product")
    public DataProduct product;
    @SerializedName("transaction")
    public Pesanan pesanan;

    protected DetailPesanan(Parcel in) {
        id = in.readInt();
        transaction_code = in.readString();
        product_code = in.readString();
        expedition = in.readString();
        quantity = in.readInt();
        weight = in.readInt();
        price = in.readInt();
        message = in.readString();
        shipping_status = in.readString();
        shipping_no = in.readString();
    }

    public Pesanan getPesanan() {
        return pesanan;
    }

    public void setPesanan(Pesanan pesanan) {
        this.pesanan = pesanan;
    }

    public DataProduct getProduct() {
        return product;
    }

    public void setProduct(DataProduct product) {
        this.product = product;
    }

    public static final Creator<DetailPesanan> CREATOR = new Creator<DetailPesanan>() {
        @Override
        public DetailPesanan createFromParcel(Parcel in) {
            return new DetailPesanan(in);
        }

        @Override
        public DetailPesanan[] newArray(int size) {
            return new DetailPesanan[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransaction_code() {
        return transaction_code;
    }

    public void setTransaction_code(String transaction_code) {
        this.transaction_code = transaction_code;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getExpedition() {
        return expedition;
    }

    public void setExpedition(String expedition) {
        this.expedition = expedition;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getShipping_status() {
        return shipping_status;
    }

    public void setShipping_status(String shipping_status) {
        this.shipping_status = shipping_status;
    }

    public String getShipping_no() {
        return shipping_no;
    }

    public void setShipping_no(String shipping_no) {
        this.shipping_no = shipping_no;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(transaction_code);
        dest.writeString(product_code);
        dest.writeString(expedition);
        dest.writeInt(quantity);
        dest.writeInt(weight);
        dest.writeInt(price);
        dest.writeString(message);
        dest.writeString(shipping_status);
        dest.writeString(shipping_no);
    }
}
