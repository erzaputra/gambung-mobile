package com.gambungstore.seller.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DataCategory implements Parcelable {

    @SerializedName("code")
    public String code;
    @SerializedName("name")
    public String name;

    protected DataCategory(Parcel in) {
        code = in.readString();
        name = in.readString();
    }

    public static final Creator<DataCategory> CREATOR = new Creator<DataCategory>() {
        @Override
        public DataCategory createFromParcel(Parcel in) {
            return new DataCategory(in);
        }

        @Override
        public DataCategory[] newArray(int size) {
            return new DataCategory[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
    }
}
