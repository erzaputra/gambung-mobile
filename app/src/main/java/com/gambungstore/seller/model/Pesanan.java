package com.gambungstore.seller.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pesanan implements Parcelable {

    @SerializedName("code")
    public String code;
    @SerializedName("username")
    public String username;
    @SerializedName("address_1")
    public String alamat;
    @SerializedName("phone")
    public String phone;
    @SerializedName("total_product")
    public int total_product;
    @SerializedName("total_quantity")
    public int total_quantity;
    @SerializedName("total_weight")
    public int total_weight;
    @SerializedName("voucher_code")
    public String voucher_code;
    @SerializedName("shipping_charges")
    public int shipping_charges;
    @SerializedName("total_amount")
    public int total_amount;
    @SerializedName("grand_total_amount")
    public int grand_total;
    @SerializedName("created_at")
    public String created_at;
    @SerializedName("detail")
    public List<DetailPesanan> detailPesanan;
    @SerializedName("payment")
    private Payment payment;

    protected Pesanan(Parcel in) {
        code = in.readString();
        username = in.readString();
        alamat = in.readString();
        phone = in.readString();
        total_product = in.readInt();
        total_quantity = in.readInt();
        total_weight = in.readInt();
        voucher_code = in.readString();
        shipping_charges = in.readInt();
        total_amount = in.readInt();
        grand_total = in.readInt();
        created_at = in.readString();
        detailPesanan = in.createTypedArrayList(DetailPesanan.CREATOR);
    }

    public static final Creator<Pesanan> CREATOR = new Creator<Pesanan>() {
        @Override
        public Pesanan createFromParcel(Parcel in) {
            return new Pesanan(in);
        }

        @Override
        public Pesanan[] newArray(int size) {
            return new Pesanan[size];
        }
    };

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getTotal_product() {
        return total_product;
    }

    public void setTotal_product(int total_product) {
        this.total_product = total_product;
    }

    public int getTotal_quantity() {
        return total_quantity;
    }

    public void setTotal_quantity(int total_quantity) {
        this.total_quantity = total_quantity;
    }

    public int getTotal_weight() {
        return total_weight;
    }

    public void setTotal_weight(int total_weight) {
        this.total_weight = total_weight;
    }

    public String getVoucher_code() {
        return voucher_code;
    }

    public void setVoucher_code(String voucher_code) {
        this.voucher_code = voucher_code;
    }

    public int getShipping_charges() {
        return shipping_charges;
    }

    public void setShipping_charges(int shipping_charges) {
        this.shipping_charges = shipping_charges;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public int getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(int grand_total) {
        this.grand_total = grand_total;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<DetailPesanan> getDetailPesanan() {
        return detailPesanan;
    }

    public void setDetailPesanan(List<DetailPesanan> detailPesanan) {
        this.detailPesanan = detailPesanan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(username);
        dest.writeString(alamat);
        dest.writeString(phone);
        dest.writeInt(total_product);
        dest.writeInt(total_quantity);
        dest.writeInt(total_weight);
        dest.writeString(voucher_code);
        dest.writeInt(shipping_charges);
        dest.writeInt(total_amount);
        dest.writeInt(grand_total);
        dest.writeTypedList(detailPesanan);
        dest.writeString(created_at);
    }
}
