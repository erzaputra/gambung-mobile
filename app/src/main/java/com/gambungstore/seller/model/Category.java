package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category {

    @SerializedName("data")
    private List<DataCategory> data;

    public List<DataCategory> getData() {
        return data;
    }

    public void setData(List<DataCategory> data) {
        this.data = data;
    }
}
