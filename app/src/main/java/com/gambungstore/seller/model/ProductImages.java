package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

public class ProductImages  {

    @SerializedName("id")
    private int id;
    @SerializedName("product_code")
    private String product_code;
    @SerializedName("image_name")
    private String image_name;
    @SerializedName("main_image")
    private String main_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

}
