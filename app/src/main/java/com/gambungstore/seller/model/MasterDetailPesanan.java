package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasterDetailPesanan {

    @SerializedName("data")
    private List<DetailPesanan> detailPesanan;

    public List<DetailPesanan> getDetailPesanan() {
        return detailPesanan;
    }

    public void setDetailPesanan(List<DetailPesanan> detailPesanan) {
        this.detailPesanan = detailPesanan;
    }
}
