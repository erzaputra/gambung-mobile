package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

public class Payment {

    @SerializedName("transaction_code")
    private String transaction_code;
    @SerializedName("proof_image")
    private String proof_image;
    @SerializedName("verified_status")
    private String verified_status;
    @SerializedName("verified_date")
    private String verified_date;

    public String getVerified_date() {
        return verified_date;
    }

    public void setVerified_date(String verified_date) {
        this.verified_date = verified_date;
    }

    public String getTransaction_code() {
        return transaction_code;
    }

    public void setTransaction_code(String transaction_code) {
        this.transaction_code = transaction_code;
    }

    public String getProof_image() {
        return proof_image;
    }

    public void setProof_image(String proof_image) {
        this.proof_image = proof_image;
    }

    public String getVerified_status() {
        return verified_status;
    }

    public void setVerified_status(String verified_status) {
        this.verified_status = verified_status;
    }
}
