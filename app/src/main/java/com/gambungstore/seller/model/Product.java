package com.gambungstore.seller.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {

    @SerializedName("data")
    private List<DataProduct> dataProduct;

    public List<DataProduct> getDataProduct() {
        return dataProduct;
    }

    public void setDataProduct(List<DataProduct> dataProduct) {
        this.dataProduct = dataProduct;
    }
}
