package com.gambungstore.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.Login;
import com.gambungstore.seller.model.Users;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "LoginActivity";
    private List<Users> usersList;
    private Service userService;

    private ProgressBarGambung progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressbar = new ProgressBarGambung(this);
        progressbar.startProgressBarGambung();

        Button btnMoveActivity = findViewById(R.id.btn_login);
        btnMoveActivity.setOnClickListener(this);


        if(getIntent().getBooleanExtra("logout",false)){
            SharedPreference.clearRegisteredId(this);
            SharedPreference.clearRegisteredStore(this);
            SharedPreference.clearRegisteredToken(this);
        }


        this.userService = Client.getClient(Client.BASE_URL).create(Service.class);
        this.refresh();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                String email = ((EditText)findViewById(R.id.input_email)).getText().toString();
                String password = ((EditText) findViewById(R.id.input_password)).getText().toString();

                if (email.isEmpty()){
                    Toast.makeText(this, "Email belum terisi", Toast.LENGTH_SHORT).show();
                    return;
                }else if(password.isEmpty()){
                    Toast.makeText(this, "Password belum terisi", Toast.LENGTH_SHORT).show();
                    return;
                }

                authLogin(email, password);
                break;
        }

    }

    private void refresh() {
        Call<List<Users>> dataUser = userService.getUsers();
        dataUser.enqueue(new Callback<List <Users>>() {
            @Override
            public void onResponse(Call<List <Users>> call, Response<List <Users>>
                    response) {
                usersList = response.body();
                int sharedId = SharedPreference.getRegisteredId(LoginActivity.this);
                boolean login = false;
                for (Users s: usersList) {
                    if (s.getId() == sharedId) {
                        login(SharedPreference.getRegisteredToken(getBaseContext()),s);
                        login = true;
                        return;
                    }
                }
                if (!login){
                    progressbar.endProgressBarGambung();
                }
            }

            @Override
            public void onFailure(Call<List <Users>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Anda tidak terkoneksi internet", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void login(String token, Users id){

        if (id.getStore() == null) {
            Toast.makeText(this, "Anda belum membuat toko", Toast.LENGTH_SHORT).show();
            progressbar.endProgressBarGambung();
            return;
        }

        progressbar.startProgressBarGambung();
        SharedPreference.setRegisteredId(this,id.getId());
        SharedPreference.setRegisteredToken(this,token);
        SharedPreference.setRegisteredStore(this,id.getStore().getCode());
        Intent move = new Intent(LoginActivity.this, MainActivity.class);
        move.putExtra("user",id);
        move.putExtra("token",token);
        move.putExtra("store", id.getStore().getCode());
        startActivity(move);
        finish();
        progressbar.endProgressBarGambung();
    }

    private void authLogin(String email, String password){
        progressbar.startProgressBarGambung();
        Call<Login> dataLogin = userService.getLogin(
                email,
                password,
                "seller"
        );
        dataLogin.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                Log.d(TAG, "onResponse: "+response.raw());
                if (response.code() == 422){
                    Toast.makeText(LoginActivity.this, "User tidak ditemukan", Toast.LENGTH_SHORT).show();
                    progressbar.endProgressBarGambung();
                }else if(response.code() == 500){
                    Toast.makeText(LoginActivity.this, "Terjadi kesalahan, silahkan dicoba beberapa saat lagi.", Toast.LENGTH_SHORT).show();
                    progressbar.endProgressBarGambung();
                } else{
                    for (Users s: usersList){
                        if (s.getEmail().equals(email)){
                            login(response.body().getToken(),s);
                            return;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progressbar.endProgressBarGambung();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
