package com.gambungstore.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gambungstore.seller.InputProductActivity;
import com.gambungstore.seller.R;
import com.gambungstore.seller.model.DataProduct;

import java.util.ArrayList;
import java.util.List;

import static com.gambungstore.seller.network.Client.IMAGE_PRODUCT_URL;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> implements Filterable {
    private static final String TAG = "ProductAdapter";
    
        List<DataProduct> productList = new ArrayList<>();
        List<DataProduct> productListCopy;
        Context context;

    public ProductAdapter(List <DataProduct> productList, Context context) {
        this.productList = productList;
        this.productListCopy = new ArrayList<>(productList);
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder (ViewGroup parent, int viewType){
            View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
            MyViewHolder mViewHolder = new MyViewHolder(mView);
            return mViewHolder;
    }

    @Override
    public void onBindViewHolder (MyViewHolder holder,final int position){
            final DataProduct productPosition = productList.get(position);
            holder.mTextViewStock.setText("Stock "+productPosition.getStock());
            holder.mTextViewName.setText(productPosition.getName());
            holder.mTextViewPrice.setText(productPosition.getPrice());
            if (!productPosition.getImage().isEmpty()) {
                Glide.with(this.context)
                        .load(IMAGE_PRODUCT_URL + productPosition.getImage().get(0).getImage_name())
                        .centerCrop()
                        .into(holder.mImageView);
            }
            holder.mBtnEdit.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent move = new Intent(v.getContext(), InputProductActivity.class);
                    move.putExtra("data", productPosition);
                    Log.d(TAG, "onClick: "+productPosition.getId());
                    if (!productPosition.getImage().isEmpty()) {
                        move.putExtra("id_product", Integer.toString(productPosition.getId()));
                        move.putExtra("image", productPosition.getImage().get(0).getImage_name());
                    }else{
                        move.putExtra("id_product", "");
                        move.putExtra("image", "");
                    }
                    move.putExtra("intent","edit");
                    v.getContext().startActivity(move);
                }
            });
        }

    @Override
    public int getItemCount () {
        return productList.size();
    }

    @Override
    public Filter getFilter() {
        return filterProduct;
    }

    private Filter filterProduct = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DataProduct> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(productListCopy);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (DataProduct item : productListCopy) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            productList.clear();
            productList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewStock, mTextViewName, mTextViewPrice;
        public ImageView mImageView;
        public Button mBtnEdit;


        public MyViewHolder(View itemView) {
            super(itemView);
            mTextViewStock = (TextView) itemView.findViewById(R.id.tvStock);
            mTextViewName = (TextView) itemView.findViewById(R.id.tvName);
            mTextViewPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            mImageView = (ImageView) itemView.findViewById(R.id.productImage);
            mBtnEdit = itemView.findViewById(R.id.btn_edit);
        }
    }
}
