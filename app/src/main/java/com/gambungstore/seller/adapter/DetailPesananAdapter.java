package com.gambungstore.seller.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gambungstore.seller.DetailPesananActivity;
import com.gambungstore.seller.R;
import com.gambungstore.seller.model.DetailPesanan;

import java.util.List;

public class DetailPesananAdapter extends RecyclerView.Adapter<DetailPesananAdapter.MyViewHolder> {
    private static final String TAG = "DetailPesananAdapter";

    private List<DetailPesanan> detailPesanan;
    private Context context;

    public DetailPesananAdapter(List<DetailPesanan> detailpesanan, Context context) {
        this.detailPesanan = detailpesanan;
        this.context = context;
    }

    @NonNull
    @Override
    public DetailPesananAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_detailpesanan, parent, false);
        DetailPesananAdapter.MyViewHolder mViewHolder = new DetailPesananAdapter.MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DetailPesananAdapter.MyViewHolder holder, int position) {
        final DetailPesanan detailPesananPosision = detailPesanan.get(position);
        Log.d(TAG, "onBindViewHolder: "+detailPesananPosision.getPrice());
        holder.mHarga.setText(String.valueOf(detailPesananPosision.getPrice()));
        holder.mQuantity.setText(String.valueOf(detailPesananPosision.getQuantity()));
        holder.mProduk.setText(detailPesananPosision.getProduct().getName());
        ((DetailPesananActivity)context).setTotal(detailPesananPosision.getQuantity()*detailPesananPosision.getPrice());
    }

    @Override
    public int getItemCount() {
        return detailPesanan.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mProduk,mQuantity,mHarga;
        Button mConfirm;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mProduk = itemView.findViewById(R.id.tv_namaProduk);
            mQuantity = itemView.findViewById(R.id.tv_quantity);
            mHarga = itemView.findViewById(R.id.tv_hargaProduk);
            mConfirm = itemView.findViewById(R.id.btn_detailConfirm);
        }
    }
}
