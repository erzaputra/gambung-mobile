package com.gambungstore.seller.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gambungstore.seller.DetailPesananActivity;
import com.gambungstore.seller.MainActivity;
import com.gambungstore.seller.R;
import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.DetailPesanan;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananAdapter extends RecyclerView.Adapter<PesananAdapter.MyViewHolder> implements Filterable {

    private static final String TAG = "PesananAdapter";
    
    List<DetailPesanan> pesananList;
    List<DetailPesanan> pesananListCopy;
    Context context;

    private String mName;
    private String noResi;
    private FragmentManager fragmentManager;

    private String from_date, until_date;

    public PesananAdapter(List<DetailPesanan> pesanan, Context context, FragmentManager fragmentManager, String from_date, String until_date) {
        this.pesananList = pesanan;
        this.pesananListCopy = new ArrayList<>(pesanan);
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.from_date = from_date;
        this.until_date = until_date;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    @NonNull
    @Override
    public PesananAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pesanan, parent, false);
        PesananAdapter.MyViewHolder mViewHolder = new PesananAdapter.MyViewHolder(mView);
        return mViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull PesananAdapter.MyViewHolder holder, int position) {
        final DetailPesanan pesananPosition = pesananList.get(position);
        holder.mPembeli.setText(pesananPosition.getPesanan().getUsername());
        holder.mNamaToko.setText(pesananPosition.getProduct().getStore().getName());

        if (from_date != null && until_date != null){
            Date from, until;
            try {
                from = new SimpleDateFormat("yyyy-MM-dd").parse(from_date);
                until = new SimpleDateFormat("yyyy-MM-dd").parse(until_date);
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pesananPosition.getPesanan().getPayment().getVerified_date());
                if (!((date.before(until) || date.equals(until))&& (date.after(from) || date.equals(from)))) {
                    holder.cardView.setVisibility(View.GONE);
                    holder.cardView.getLayoutParams().height = 0;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(!pesananPosition.getShipping_status().equals("OPTNO")){
            holder.mConfirm.setText("Sudah Konfirmasi");
            holder.mConfirm.setEnabled(false);
            holder.mConfirm.setBackgroundColor(this.context.getResources().getColor(R.color.colorPrimary));
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime verified_date = LocalDateTime.parse(pesananPosition.getPesanan().getPayment().getVerified_date(),formatter);
        LocalDateTime now = LocalDateTime.now();
        long days = ChronoUnit.DAYS.between(verified_date, now);
        Log.d(TAG, "onBindViewHolder: "+days);
        if (days >= 1 && (pesananPosition.getShipping_status().equals("OPTNO") || pesananPosition.getShipping_status().equals("OPTFL"))){
            holder.mConfirm.setText("Kadaluarsa");
            holder.mConfirm.setEnabled(false);
            holder.mConfirm.setBackgroundColor(this.context.getResources().getColor(R.color.colorPrimary));
        }

        holder.mDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent move = new Intent(v.getContext(), DetailPesananActivity.class);
                move.putExtra("id", pesananPosition.getId());
                move.putExtra("transaction_code", pesananPosition.getTransaction_code());
                move.putExtra("transaction_detail_id", pesananPosition.getId());
                v.getContext().startActivity(move);
            }
        });

        holder.mLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pesananPosition.getPesanan().getPayment().getProof_image() == null){
                    Toast.makeText(context, "Pembayaran Menggunakan Ji-Cash", Toast.LENGTH_SHORT).show();
                    return;
                }
                Dialog settingsDialog = new Dialog(context);
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layoutPopup = li.inflate(R.layout.popup_image,null);
                ImageView imagePopup = layoutPopup.findViewById(R.id.bukti_transaksi);
                if (pesananPosition.getPesanan().getPayment().getVerified_status().equals("OPTYS")){
                    Log.d(TAG, "onClick: "+Client.IMAGE_PROOF_URL
                            +pesananPosition.getPesanan().getPayment().getProof_image());
                    Glide.with(v).load(Client.IMAGE_PROOF_URL
                            +pesananPosition.getPesanan().getPayment().getProof_image()).into(imagePopup);
                }else{
                    imagePopup.setImageResource(R.drawable.gambung_coklat);
                }

                settingsDialog.setContentView(layoutPopup);
                settingsDialog.show();
            }
        });

        holder.mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Masukan Nomor Resi");

                // Set up the input
                final EditText input = new EditText(v.getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        noResi = input.getText().toString();
                        confirmation(pesananPosition.getTransaction_code(), pesananPosition.getId(), noResi);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return pesananList.size();
    }

    @Override
    public Filter getFilter() {
        return filterPesanan;
    }

    private Filter filterPesanan = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DetailPesanan> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(pesananListCopy);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (DetailPesanan item : pesananListCopy) {
                    if (item.getPesanan().getUsername().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            pesananList.clear();
            pesananList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mLihat,mDetail,mNamaToko,mPembeli;
        Button mConfirm;
        LinearLayout cardView;

        public MyViewHolder(View mView) {
            super(mView);
            mLihat = mView.findViewById(R.id.tv_lihat);
            mDetail = mView.findViewById(R.id.tv_detail);
            mNamaToko = mView.findViewById(R.id.tv_namaToko);
            mPembeli = mView.findViewById(R.id.tv_pembeli);
            mConfirm = mView.findViewById(R.id.btn_confirm);
            cardView = mView.findViewById(R.id.cardView);
        }
    }

    private void confirmation(String transaction_code, int transaction_detail_id, String shipping_no){

        ProgressBarGambung progress = new ProgressBarGambung((MainActivity)context);
        progress.startProgressBarGambung();

        Log.d(TAG, "detail id: " + transaction_detail_id);

        Service service = Client.getClient(Client.BASE_URL).create(Service.class);
        Call<ResponseBody> confirm = service.confirmPesanan(
                "Bearer "+SharedPreference.getRegisteredToken(context),
                transaction_code,
                transaction_detail_id,
                shipping_no
        );

        confirm.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(context, "Berhasil Dikonfirmasi", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent((MainActivity)context,MainActivity.class);
                intent.putExtra("fragment","pesanan");
                context.startActivity(intent);
                ((MainActivity)context).finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                progress.endProgressBarGambung();
            }
        });
    }
}
