package com.gambungstore.seller.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gambungstore.seller.R;
import com.gambungstore.seller.model.ProductImages;
import com.gambungstore.seller.network.Client;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private static final String TAG = "ImageAdapter";

    List<ProductImages> productImagesList;
    Context context;

    public ImageAdapter(List<ProductImages> productImagesList, Context context){
        this.productImagesList = productImagesList;
        this.context = context;
    }

    @NonNull
    @Override
    public ImageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_imageproduct, parent, false);
        ImageAdapter.MyViewHolder mViewHolder = new ImageAdapter.MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.MyViewHolder holder, int position) {
        ProductImages productImagesPosition = productImagesList.get(position);

        Glide.with(context)
                .load(Client.IMAGE_PRODUCT_URL+productImagesPosition.getImage_name())
                .apply(new RequestOptions().override(150, 150))
                .into(holder.mImage);

        Log.d(TAG, "onBindViewHolder: disini");

        if (productImagesPosition.getMain_image().matches("OPTNO")){

        }
    }

    @Override
    public int getItemCount() {
        return productImagesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView mImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mImage = itemView.findViewById(R.id.imageViewAdapter);
        }
    }
}
