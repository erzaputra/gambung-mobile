package com.gambungstore.seller.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gambungstore.seller.R;
import com.gambungstore.seller.model.CategoryStatus;

import java.util.ArrayList;
import java.util.List;

public class StatusProductAdapter extends RecyclerView.Adapter<StatusProductAdapter.MyViewHolder> {

    private static final String TAG = "StatusProductAdapter";
    private List<CategoryStatus> statusList = new ArrayList<>();
        private Context context;

    public StatusProductAdapter(List<CategoryStatus> statusList, Context context){
        this.statusList = statusList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_status, parent, false);
        StatusProductAdapter.MyViewHolder mViewHolder = new StatusProductAdapter.MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final CategoryStatus statusPosition = statusList.get(position);
        Log.d(TAG, "onBindViewHolder: "+statusPosition.getStatus().getName());
        holder.tv_status.setText(statusPosition.getStatus().getName());
    }


    @Override
    public int getItemCount() {
        return statusList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public EditText mStatus;
        public TextView tv_status;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mStatus = itemView.findViewById(R.id.et_status);
            tv_status = itemView.findViewById(R.id.tv_status);
        }
    }
}
