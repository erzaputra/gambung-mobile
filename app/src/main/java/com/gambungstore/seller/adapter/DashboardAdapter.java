package com.gambungstore.seller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gambungstore.seller.R;
import com.gambungstore.seller.model.DetailPesanan;

import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder>  {

    private List<DetailPesanan> pesanan;
    private Context context;

    public DashboardAdapter(List<DetailPesanan> pesanan, Context context){
        this.pesanan = pesanan;
        this.context = context;
    }

    public MyViewHolder onCreateViewHolder (ViewGroup parent, int viewType){
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_orderterkini, parent, false);
        MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DetailPesanan pesananPosition = pesanan.get(position);
        holder.mCode.setText(pesananPosition.getTransaction_code());
        holder.mHarga.setText(String.valueOf(pesananPosition.getPrice()));
        holder.mNama.setText(pesananPosition.getPesanan().getUsername());
        holder.mTanggal.setText(pesananPosition.getPesanan().getCreated_at());
    }

    @Override
    public int getItemCount() {
        return pesanan.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView mCode, mNama, mHarga, mTanggal;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mCode = itemView.findViewById(R.id.code_transaction);
            mNama = itemView.findViewById(R.id.nama);
            mHarga = itemView.findViewById(R.id.harga);
            mTanggal = itemView.findViewById(R.id.tanggal);
        }
    }
}
