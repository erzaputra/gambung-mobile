package com.gambungstore.seller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;

import android.view.MenuItem;


import com.gambungstore.seller.model.Users;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_bot);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        //pengecekan parsing data menggunakan intent extra
        if (getIntent() != null) {
            Users user = getIntent().getParcelableExtra("user");
            if (getIntent().getStringExtra("fragment") != null){
                if (getIntent().getStringExtra("fragment").equals("pesanan")) {
                    Fragment selectedFragment = new FragmentPesanan();
                    bottomNavigationView.setSelectedItemId(R.id.nav_pesanan);
                    return;
                }
            }
        }

        Fragment selectedFragment = new FragmentDashboard();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment selectedFragment = null;
        switch (menuItem.getItemId()) {
            case R.id.nav_dashboard:
                selectedFragment = new FragmentDashboard();
                break;
            case R.id.nav_produk:
                selectedFragment = new FragmentProduct();
                break;
            case R.id.nav_pesanan:
                selectedFragment = new FragmentPesanan();
                break;
            case R.id.nav_pendapatan:
                selectedFragment = new fragment_transaksi();
                break;
            case R.id.nav_profil:
                selectedFragment = new FragmentProfile();
                break;

        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,selectedFragment).commit();
        return true;
    }

}
