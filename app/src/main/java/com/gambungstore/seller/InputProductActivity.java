package com.gambungstore.seller;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gambungstore.seller.adapter.ImageAdapter;
import com.gambungstore.seller.adapter.StatusProductAdapter;
import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.Category;
import com.gambungstore.seller.model.CategoryStatus;
import com.gambungstore.seller.model.DataCategory;
import com.gambungstore.seller.model.DataProduct;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputProductActivity extends AppCompatActivity  {

    private static final String TAG = "InputProductActivity";

    private final int REQUEST_CODE_CAMERA = 001;
    private final int REQUEST_CODE_GALLERY = 002;

    private List<DataCategory> categoryList;
    private Service service;

    //xmls
    private Spinner mSpinnerKategori;
    private String code_category;
    private TextView nextBtn,headerInputProduct;
    private Button btnDelete;
    private ImageButton btnUpload;
    private ImageView imageUpload;
    private EditText nama_produk,deskripsi,stock,berat,harga;
    private LinearLayout layoutHarga,layoutBerat,layoutStock;

    //model
    DataProduct product;

    //adapter
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    //picture
    private File file;
    private RequestBody requestFile;
    private MultipartBody.Part body;

    String condition;

    ProgressBarGambung progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_product);

        progressBar = new ProgressBarGambung(this);
        progressBar.startProgressBarGambung();
        condition = getIntent().getStringExtra("intent");

        this.service = Client.getClient(Client.BASE_URL).create(Service.class);
        this.getAllXml();
        this.showCategory();

        Log.d(TAG, "onCreate: "+condition);
        if(condition.equals("edit")){
            product = getIntent().getParcelableExtra("data");
            Log.d(TAG, "onCreate: check "+product.getName());
            this.showAllStatus();
            btnDelete.setOnClickListener(this::onClickInputProduct);
            getIntent().getStringExtra("image");
        }else{
            this.btnUpload.setVisibility(View.VISIBLE);
            this.mRecyclerView.setVisibility(View.GONE);
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInputProduct(v);
            }
        });
        progressBar.endProgressBarGambung();
    }

    private void getAllXml(){
        nextBtn = findViewById(R.id.btn_selanjutnya);
        layoutHarga = findViewById(R.id.layout_harga);
        layoutBerat = findViewById(R.id.layout_berat);
        layoutStock = findViewById(R.id.layout_stock);
        mSpinnerKategori = findViewById(R.id.spinner_kategori);
        nama_produk = findViewById(R.id.et_namaproduk);
        deskripsi = findViewById(R.id.et_deskripsi);
        stock = findViewById(R.id.et_stock);
        harga = findViewById(R.id.et_hargaproduk);
        berat = findViewById(R.id.et_berat);
        btnDelete = findViewById(R.id.btn_delete);
        btnUpload = findViewById(R.id.btn_upload);
        imageUpload = findViewById(R.id.imageUpload);
        headerInputProduct = findViewById(R.id.headerInput);
        mRecyclerView = findViewById(R.id.recyclerView);
    }

    //menampilkan category
    private void showCategory(){
        Call<Category> dataCategory = service.getCategory();
        dataCategory.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category>
                    response) {
                categoryList = response.body().getData();

                List<String> SpinnerCategories = new ArrayList<String>();
                SpinnerCategories.add("Pilih Kategori");

                int position = 0, i = 0;
                for (DataCategory category : categoryList){
                    SpinnerCategories.add(category.getName());
                    i++;
                    if(condition.equals("edit")) {
                        if (category.getCode().equals(product.getCategory())) {
                            position = i;
                        }
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                        android.R.layout.simple_spinner_item, SpinnerCategories);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mSpinnerKategori.setAdapter(adapter);
                code_category = null;

                if(condition.equals("edit")){
                    mSpinnerKategori.setSelection(position);
                }

                mSpinnerKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        if (position == 0){
                            code_category = null;
                        }else{
                            code_category = categoryList.get(position-1).getCode();
                            Log.d(TAG, "onItemSelected() returned: " + code_category);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        return;
                    }

                });
            }
            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progressBar.endProgressBarGambung();
            }
        });

    }

    //kondisi jika menekan button
    public void onClickInputProduct(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_selanjutnya:
                if (code_category == null) {
                    Toast.makeText(this, "Anda belum memilih kategori", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (nama_produk.getText().toString().matches("") || deskripsi.getText().toString().matches("")) {
                    Toast.makeText(this, "Data anda belum lengkap, silahkan diisi", Toast.LENGTH_SHORT).show();
                    break;
                    }

                if (condition.equals("input")) {
                    //apakah button tersebut masih selanjutnya atau sudah selesai
                    if (nextBtn.getText().toString().matches("Selanjutnya")) {
                        showStatus();
                    } else {
                        if (stock.getText().toString().matches("") ||
                                harga.getText().toString().matches("") ||
                                berat.getText().toString().matches("")) {
                            Toast.makeText(this, "Status anda belum diisi", Toast.LENGTH_SHORT).show();
                            break;
                        }

                        Log.d(TAG, "onClickInputProduct: "+SharedPreference.getRegisteredStore(this)+" "+nama_produk.getText().toString()+" "
                        +code_category+" "+deskripsi.getText().toString()+" "+berat.getText().toString()+" "+stock.getText().toString()+" "
                        +harga.getText().toString()+" "+body);

                        RequestBody store_code = RequestBody.create(MediaType.parse("text/plain"), SharedPreference.getRegisteredStore(this));
                        RequestBody product_name = RequestBody.create(MediaType.parse("text/plain"), nama_produk.getText().toString());
                        RequestBody kode_kategori = RequestBody.create(MediaType.parse("integer"), code_category);
                        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), deskripsi.getText().toString());
                        RequestBody brt = RequestBody.create(MediaType.parse("text/plain"), berat.getText().toString());
                        RequestBody stk = RequestBody.create(MediaType.parse("integer"), stock.getText().toString());
                        RequestBody hrg = RequestBody.create(MediaType.parse("text/plain"), harga.getText().toString());

                        progressBar.startProgressBarGambung();

                        Call<ResponseBody> postProduct = service.storeProduct(
                                store_code,
                                product_name,
                                kode_kategori,
                                desc,
                                brt,
                                stk,
                                hrg,
                                this.body
                        );
                        postProduct.enqueue(new Callback<ResponseBody>() {

                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                Log.d(TAG, "onResponse post product: "+response.raw());
                                if  (response.code() == 201){
                                    Toast.makeText(InputProductActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
                                    finish();
                                }else{
                                    Toast.makeText(InputProductActivity.this, "Terjadi Kesalahan, pastikan seluruh data termasuk gambar sudah diisi dengan benar", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                                progressBar.endProgressBarGambung();
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                                progressBar.endProgressBarGambung();
                            }
                        });
                    }
                }else{

                    if (stock.getText().toString().matches("") ||
                            harga.getText().toString().matches("") ||
                            berat.getText().toString().matches("")) {
                        Toast.makeText(this, "Status anda belum diisi", Toast.LENGTH_SHORT).show();
                        break;
                    }

                    service = Client.getClient(Client.BASE_URL).create(Service.class);

                    tambahImage();

                    progressBar.startProgressBarGambung();
                    Call<ResponseBody> updateProcess = service.updateProduct(
                            product.getId(),
                            nama_produk.getText().toString(),
                            code_category,
                            code_category,
                            deskripsi.getText().toString(),
                            Integer.valueOf(berat.getText().toString()),
                            Integer.valueOf(stock.getText().toString()),
                            Integer.valueOf(harga.getText().toString())
                    );
                    updateProcess.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Toast.makeText(InputProductActivity.this, "Berhasil diubah", Toast.LENGTH_SHORT).show();
                            finish();
                            progressBar.endProgressBarGambung();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                            progressBar.endProgressBarGambung();
                        }
                    });
                }
                break;
            case R.id.btn_delete:
                delete();
                break;
        }

    }

    private void tambahImage(){

        Log.d(TAG, "tambahImage: "+body);

        Call<ResponseBody> callTambahImage = service.createImage(
                RequestBody.create(MediaType.parse("text/plain"),product.getCode()),
                this.body,
                RequestBody.create(MediaType.parse("text/plain"),"OPTNO")
        );

        callTambahImage.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onResponse: upload image  "+response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }

    //deleted
    private void delete(){
        android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setMessage("Product yang dihapus tidak akan dapat dikembalikan lagi.");
        alert.setTitle("Apakah Anda Yakin ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                progressBar.startProgressBarGambung();
                Call<ResponseBody> deleteProcess = service.deleteProduct(product.getId());
                deleteProcess.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.d(TAG, "onResponses: "+response.raw());
                        if(response.code() == 500){
                            Toast.makeText(InputProductActivity.this, "Barang dalam transaksi, tidak dapat dihapus", Toast.LENGTH_SHORT).show();
                        }else if(response.code() == 202){
                            Toast.makeText(InputProductActivity.this, "Barang diubah stocknya menjadi 0", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(InputProductActivity.this, "Berhasil dihapus", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        progressBar.endProgressBarGambung();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                        progressBar.endProgressBarGambung();
                    }
                });
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.show();
    }

    private void showStatus(){
        if (code_category != null) {

            mSpinnerKategori.setEnabled(false);

            progressBar.startProgressBarGambung();

            Call<List<CategoryStatus>> dataCategoryStatus = service.categoryStatus(
              this.code_category
            );
            dataCategoryStatus.enqueue(new Callback<List<CategoryStatus>>() {
                @Override
                public void onResponse(Call<List<CategoryStatus>> call, Response<List<CategoryStatus>> response) {

                    Log.d(TAG, "onResponse: "+response.body());

                    nextBtn.setText("Selesai");
                    layoutHarga.setVisibility(View.VISIBLE);
                    layoutBerat.setVisibility(View.VISIBLE);
                    btnUpload.setVisibility(View.VISIBLE);
                    headerInputProduct.setVisibility(View.GONE);

                    mRecyclerView = findViewById(R.id.recyclerViewStatus);
                    mLayoutManager = new LinearLayoutManager(InputProductActivity.this);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new StatusProductAdapter(response.body(), InputProductActivity.this);
                    mRecyclerView.setAdapter(mAdapter);
                    layoutStock.setVisibility(View.VISIBLE);

                    progressBar.endProgressBarGambung();

                }

                @Override
                public void onFailure(Call<List<CategoryStatus>> call, Throwable t) {
                    Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                    progressBar.endProgressBarGambung();
                }
            });

        }
    }

    private void showAllStatus(){
        nextBtn.setText("Selesai");

        mSpinnerKategori.setEnabled(false);

        if(!getIntent().getStringExtra("id_product").isEmpty()){

            Log.d(TAG, "showAllStatus: "+getIntent().getStringExtra("id_product"));
            this.service = Client.getClient(Client.BASE_URL).create(Service.class);
            Call<DataProduct> productList = service.getProductByID(
                    Integer.valueOf(getIntent().getStringExtra("id_product"))
            );
            productList.enqueue(new Callback<DataProduct>() {
                @Override
                public void onResponse(Call<DataProduct> call, Response<DataProduct> response) {
                    Log.d(TAG, "onResponse: "+response.raw());
                    DataProduct dataProductAPI = response.body();

                    mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                    mLayoutManager = new LinearLayoutManager(InputProductActivity.this,LinearLayoutManager.HORIZONTAL, false);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new ImageAdapter(dataProductAPI.getImage(), InputProductActivity.this);
                    mRecyclerView.setAdapter(mAdapter);

                }

                @Override
                public void onFailure(Call<DataProduct> call, Throwable t) {
                    Toast.makeText(InputProductActivity.this, "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                    progressBar.endProgressBarGambung();
                }
            });
        }

        btnUpload.setVisibility(View.VISIBLE);
        headerInputProduct.setVisibility(View.GONE);
        layoutHarga.setVisibility(View.VISIBLE);
        layoutBerat.setVisibility(View.VISIBLE);
        layoutStock.setVisibility(View.VISIBLE);
        btnDelete.setVisibility(View.VISIBLE);

        nama_produk.setText(product.getName());
        deskripsi.setText(product.getDesctiption());
        harga.setText(product.getPrice());
        stock.setText(String.valueOf(product.getStock()));
        berat.setText(String.valueOf(product.getWeight()));
    }

    private void uploadImage(){
        CharSequence[] item = {"Kamera", "Galeri"};
        AlertDialog.Builder request = new AlertDialog.Builder(this)
                .setTitle("Add Image")
                .setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i){
                            case 0:
                                //Membuka Kamera Untuk Mengambil Gambar
                                EasyImage.openCamera(InputProductActivity.this, REQUEST_CODE_CAMERA);
                                break;
                            case 1:
                                //Membuaka Galeri Untuk Mengambil Gambar
                                EasyImage.openGallery(InputProductActivity.this, REQUEST_CODE_GALLERY);
                                break;
                        }
                    }
                });
        request.create();
        request.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new EasyImage.Callbacks() {

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Method Ini Digunakan Untuk Menghandle Error pada Image
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                //Method Ini Digunakan Untuk Menghandle Image

                Log.d(TAG, "onImagePicked: "+imageFile+" "+source+" "+type);

                imageUpload.setVisibility(View.VISIBLE);
                Glide.with(InputProductActivity.this)
                        .load(imageFile)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageUpload);

                try {
                    file = new File(String.valueOf(imageFile));
                    File compressedImgFile = new Compressor(getBaseContext()).compressToFile(file);
                    requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), compressedImgFile);
                    String imageName = "/"+file.getName();
                    body = MultipartBody.Part.createFormData("image", imageName, requestFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Batalkan penanganan, Anda mungkin ingin menghapus foto yang diambil jika dibatalkan
            }
        });
    }
}

