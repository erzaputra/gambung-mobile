package com.gambungstore.seller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


import com.gambungstore.seller.adapter.ProductAdapter;
import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.model.DataProduct;
import com.gambungstore.seller.model.Product;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProduct extends Fragment {

    private static final String TAG = "FragmentProduct";
    private List<DataProduct> productList = new ArrayList<>();
    private Service productService;
    private RecyclerView mRecyclerView;
    private ProductAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ProgressBarGambung progressbar;

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressbar = new ProgressBarGambung(getActivity());
        progressbar.startProgressBarGambung();

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        this.productService = Client.getClient(Client.BASE_URL).create(Service.class);
        this.getAdapter();

        //floating action tambah product
        FloatingActionButton tambahProduct =(FloatingActionButton) getActivity().findViewById(R.id.btn_tambah);
        tambahProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent move = new Intent(getActivity(), InputProductActivity.class);
                move.putExtra("intent", "input");
                startActivity(move);
            }
        });

        EditText mSearch = getActivity().findViewById(R.id.search);
        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().isEmpty()){
                    mAdapter.getFilter().filter(editable.toString());
                } else {
                    getAdapter();
                }
            }
        });

    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_product, container, false);
    }

    private void getAdapter(){
        Call<Product> dataProduct = productService.getProduct(
                SharedPreference.getRegisteredStore(getContext())
        );
        dataProduct.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if(response.body() != null) {
                    productList = response.body().getDataProduct();
                }else{
                    Toast.makeText(getContext(), "Anda belum memiliki product", Toast.LENGTH_SHORT).show();
                    return;
                }
                mAdapter = new ProductAdapter(productList, requireContext());
                mRecyclerView.setAdapter(mAdapter);
                progressbar.endProgressBarGambung();
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Toast.makeText(getContext(), "Terjadi kesalahan, silahkan coba lagi!", Toast.LENGTH_SHORT).show();
                progressbar.endProgressBarGambung();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getAdapter();
    }
}
