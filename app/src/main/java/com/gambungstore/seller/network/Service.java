package com.gambungstore.seller.network;

import android.database.Observable;

import com.gambungstore.seller.model.Category;
import com.gambungstore.seller.model.CategoryStatus;
import com.gambungstore.seller.model.Dashboard;
import com.gambungstore.seller.model.Login;
import com.gambungstore.seller.model.MasterDetailPesanan;
import com.gambungstore.seller.model.Product;
import com.gambungstore.seller.model.Rajaongkir;
import com.gambungstore.seller.model.DetailPesanan;
import com.gambungstore.seller.model.DataProduct;
import com.gambungstore.seller.model.Users;

import java.util.Date;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {

    @FormUrlEncoded
    @POST("login")
    Call <Login> getLogin(
            @Field("email") String email,
            @Field("password") String password,
            @Field("role") String role
    );

    @GET("users")
    Call <List <Users>> getUsers();

    @FormUrlEncoded
    @POST("check-password")
    Call<ResponseBody> checkOldPass(
            @Header("Authorization") String token,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("update-password")
    Call<ResponseBody> updatePass(
            @Header("Authorization") String token,
            @Field("password") String password,
            @Field("password_confirmation") String confirmPassword
    );

    @GET("product")
    Call <Product> getProduct(
            @Query("store_code") String store_code
    );

    @GET("category/status")
    Call <List <CategoryStatus>> categoryStatus(
            @Query("category_code") String category_code
    );

    @Multipart
    @POST("product")
    Call <ResponseBody> storeProduct(
            @Part("store_code") RequestBody code,
            @Part("name") RequestBody nama,
            @Part("main_category") RequestBody kategori,
            @Part("description") RequestBody deskripsi,
            @Part("weight") RequestBody berat,
            @Part("stock") RequestBody stock,
            @Part("price") RequestBody harga,
            @Part MultipartBody.Part image
            );

    @DELETE("product/{id}")
    Call <ResponseBody> deleteProduct(
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT("product/{id}")
    Call <ResponseBody> updateProduct(
            @Path("id") int id,
            @Field("name") String nama,
            @Field("main_category") String kategori,
            @Field("sub_category") String subkategori,
            @Field("description") String deskripsi,
            @Field("weight") int berat,
            @Field("stock") int stock,
            @Field("price") int harga
    );

    @GET("product/{id}")
    Call <DataProduct> getProductByID(
            @Path("id") int id
    );

    @Multipart
    @POST("product/images")
    Call <ResponseBody> createImage(
            @Part("product_code") RequestBody product_code,
            @Part MultipartBody.Part image,
            @Part("main_image") RequestBody main_image
    );

    @GET("category")
    Call <Category> getCategory();


    @GET("transaction")
    Call <MasterDetailPesanan> getPesanan(
            @Query("store_code") String store_code
    );

    @GET("pesanan")
    Call <List<DetailPesanan>> getAllPesanan();

    @Headers("key: e2f076d77998bbb2921165ee490297a4")
    @GET("city")
    Call <Rajaongkir> getRajaongkir();

    @FormUrlEncoded
    @PUT("users/{id}")
    Call <String> editProfile(
            @Path("id") String id_users,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("address") String address,
            @Field("city") int city,
            @Field("birthday") Date tgl_lahir
    );

    @GET("users/{id}")
    Call<List<Users>> getUserByID(
            @Path("id") int id
    );

    @FormUrlEncoded
    @Multipart
    @POST("product/images")
    Observable<ResponseBody> uploadImage(
      @Field("product_code") String product_code,
      @Part MultipartBody.Part image,
      @Field("main_image") String main_image
    );

    @GET("dashboard")
    Call<Dashboard> getDashboard(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("transaction/delivery_confirmation")
    Call <ResponseBody> confirmPesanan(
            @Header("Authorization") String token,
            @Field("transaction_code") String transaction_code,
            @Field("transaction_detail_id") int transaction_detail_id,
            @Field("shipping_no") String shipping_no
    );
}
