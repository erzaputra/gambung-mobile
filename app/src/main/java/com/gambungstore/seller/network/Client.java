package com.gambungstore.seller.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Client {

        public static final String BASE_URL = "https://gambungstore.id/api/";
        public static final String IMAGE_PRODUCT_URL = "https://gambungstore.id/assets/img/products";
        public static final String IMAGE_PROOF_URL = "https://gambungstore.id/assets/img/proof/";

        public static Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        private static Retrofit retrofit = null;
        public static Retrofit getClient(String url) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
            return retrofit;
        }

        public static Retrofit getRajaongkir(){
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.rajaongkir.com/starter/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }

}
