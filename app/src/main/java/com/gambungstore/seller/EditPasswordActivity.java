package com.gambungstore.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.gambungstore.seller.loadingbar.ProgressBarGambung;
import com.gambungstore.seller.network.Client;
import com.gambungstore.seller.network.Service;
import com.gambungstore.seller.sharedpreference.SharedPreference;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPasswordActivity extends AppCompatActivity {
    private static final String TAG = "EditPasswordActivity";
    
    ImageButton mBack;
    Button mUbah;
    EditText mPasswordLama, mPasswordBaru, mRepassword;

    ProgressBarGambung progressbar = new ProgressBarGambung(this);

    Service service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);

        mBack = findViewById(R.id.btn_back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mUbah = findViewById(R.id.btn_ubah);
        mUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ubahPassword();
            }
        });
    }

    private void ubahPassword(){
        progressbar.startProgressBarGambung();

        mPasswordBaru = findViewById(R.id.et_passwordBaru);
        mPasswordLama = findViewById(R.id.et_passwordLama);
        mRepassword = findViewById(R.id.et_repassword);

        String passwordLama = mPasswordLama.getText().toString();
        String passwordBaru = mPasswordBaru.getText().toString();
        String rePassword = mRepassword.getText().toString();

        if(passwordBaru.isEmpty() || passwordLama.isEmpty() || rePassword.isEmpty()){
            Toast.makeText(this, "Ada field yang belum diisi", Toast.LENGTH_SHORT).show();
            progressbar.endProgressBarGambung();
            return;
        }else if(!passwordBaru.equals(rePassword)){
            Toast.makeText(this, "Password baru dan repassword tidak sama", Toast.LENGTH_SHORT).show();
            progressbar.endProgressBarGambung();
            return;
        }

        service = Client.getClient(Client.BASE_URL).create(Service.class);
        Call<ResponseBody> checkOldPass = service.checkOldPass(
                "Bearer "+ SharedPreference.getRegisteredToken(this),
                passwordLama
        );
        checkOldPass.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    updatePass(passwordBaru, rePassword);
                } else {
                    progressbar.endProgressBarGambung();
                    Toast.makeText(EditPasswordActivity.this, "Password lama tidak sesuai", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbar.endProgressBarGambung();
                Toast.makeText(EditPasswordActivity.this, "Terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updatePass(String newPassword, String rePassword) {
        service = Client.getClient(Client.BASE_URL).create(Service.class);
        Call<ResponseBody> setPass = service.updatePass(
                "Bearer "+ SharedPreference.getRegisteredToken(this),
                newPassword,
                rePassword
        );
        setPass.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                finish();
                progressbar.endProgressBarGambung();
                Toast.makeText(EditPasswordActivity.this, "Berhasil diubah", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbar.endProgressBarGambung();
                Toast.makeText(EditPasswordActivity.this, "Terjadi kesalahan, silahkan coba lagi", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
